FROM python:slim
ADD ./code /mnt
ADD job /etc/cron.d/job
RUN chmod 0644 /etc/cron.d/job
WORKDIR /mnt
RUN chmod 0644 crontab.sh

RUN touch /var/log/cron.log
RUN apt-get update
RUN apt-get -y install cron
CMD cron && tail -f /var/log/cron.log
