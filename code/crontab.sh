#!/bin/bash

cd /mnt
echo "1"
echo $TEST ##因為在crontab的 shell, 為空
echo "============="

echo "2"
source /mnt/.env ## source env變數
echo $TEST ## 所以輸出test
echo "============="

echo "3"
export TEST="test2" ## 在crontab裡重新export
echo $TEST ##輸出test2
